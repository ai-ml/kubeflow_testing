FROM python:3.10

COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

COPY pytest_kubeflow.py /pytest_kubeflow.py
COPY pytest.ini /pytest.ini
