import os
import base64
import time
import requests
import yaml
import pytest
import kfp
from kubernetes import client, config


git_repo = 'https://gitlab.cern.ch/ai-ml/examples/-/raw/master/'
pipeline_paths = ['pipelines/argo-workflows/hello-world/dag_diamond.yaml',
                  'pipelines/argo-workflows/access_eos/access_eos.yaml']
katib_paths = ['katib/hp-tuning.yaml']
kserve_paths = ['serving/single-model/flowers.yaml']
training_operator_paths = ['training-operators/tfjob/mnist/tfjob.yaml',
                           'training-operators/pytorchjob/mnist/pytorchjob.yaml']

pipeline_urls = [git_repo + path for path in pipeline_paths]
katib_urls = [git_repo + path for path in katib_paths]
kserve_urls = [git_repo + path for path in kserve_paths]
training_operator_urls = [git_repo + path for path in training_operator_paths]

sleep_interval = 5
sleep_multplier = 1000

print(pipeline_urls)
print(katib_urls)

config.load_incluster_config()
k8s_client = client.CustomObjectsApi()

def read_content_from_url(url):
    response = requests.get(url)

    if response.status_code == 200:
        return response._content.decode('utf-8')
    
    return None
    
class TestClass:

    @pytest.mark.eos
    def test_eos(self):
        assert os.system('ls /eos/user/d/dgolubov') == 0
    
    @pytest.mark.pipelines
    @pytest.mark.parametrize("url", pipeline_urls)
    def test_pipelines(self, url):
        ts = time.time()
        content_yaml = read_content_from_url(url)
        body = yaml.load(content_yaml, yaml.FullLoader)
        body['metadata']['name'] = body['metadata']['generateName'] + 'test-' + str(ts)
        del body['metadata']['generateName']

        pipeline_file = 'pipeline.yaml'
        with open(pipeline_file, 'w') as f:
            f.write(yaml.dump(body))

        experiment_name = 'test_experiment'
        client = kfp.Client()
        client.upload_pipeline(pipeline_file, body['metadata']['name'])

        try:
            exp = client.get_experiment(experiment_name = experiment_name)
        except:
            exp = client.create_experiment(name=experiment_name)

        run = client.run_pipeline(exp.id, body['metadata']['name'], pipeline_file)
        cnt = 0
        while cnt < sleep_multplier:
            try:
                if client.get_run(run.id).run.status == 'Failed':
                    raise(Exception())

                if client.get_run(run.id).run.status == 'Succeeded':
                    print('Pipeline run succeeded')
                    break

                cnt += 1
                time.sleep(sleep_interval)

            except:
                print('Pipeline run failed')
                assert False
                
        assert True
  
    @pytest.mark.katib
    @pytest.mark.parametrize("url", katib_urls)
    def test_katib(self, url):
        content_yaml = read_content_from_url(url)
        
        api_instance = client.CustomObjectsApi()
        plural = 'experiments'
        group = 'kubeflow.org'
        version = 'v1beta1'
        namespace = 'dgolubov'
        body = yaml.load(content_yaml, yaml.FullLoader)
        
        try:
            api_response = api_instance.create_namespaced_custom_object(group, 
                                                                    version, 
                                                                    namespace, 
                                                                    plural,
                                                                    body)
        except Exception as e:
            print('Katib experiment not created')
            assert False
            
        print('Katib experiment created')
        
        cnt = 0
        
        while cnt < sleep_multplier:
            try:
                api_response = api_instance.get_namespaced_custom_object(group,
                                                                         version,
                                                                         namespace,
                                                                         plural,
                                                                         body['metadata']['name'])
            
                if 'status' in api_response:
                    if 'completionTime' in api_response['status'] and api_response['status']['completionTime']:
                        print('Katib experiment succeeded')
                        break
                        
                cnt += 1
                time.sleep(sleep_interval)
                
            except:
                print('Katib experiment failed during run')
                assert False
        
        print(cnt)
        if cnt >= sleep_multplier:
            print('Katib experiment did not succeed in', (sleep_interval * sleep_multplier), 'seconds')
            assert False
        
        try:
            api_instance.delete_namespaced_custom_object(group,
                                                         version,
                                                         namespace,
                                                         plural,
                                                         body['metadata']['name'])
        except:
            print('Katib experiment not deleted')
            assert False

        print('Katib experiment deleted')
        assert True

    @pytest.mark.kserve    
    @pytest.mark.parametrize("url", kserve_urls)    
    def test_kserve(self, url):
        content_yaml = read_content_from_url(url)
        
        api_instance = client.CustomObjectsApi()
        plural = 'inferenceservices'
        group = 'serving.kubeflow.org'
        version = 'v1beta1'
        namespace = 'dgolubov'
        body = yaml.load(content_yaml, yaml.FullLoader)
        
        try:
            api_response = api_instance.create_namespaced_custom_object(group, 
                                                                    version, 
                                                                    namespace, 
                                                                    plural,
                                                                    body)
        except Exception as e:
            print('Inferenceservice not created')
            assert False

        cnt = 0
        while cnt < sleep_multplier:
            try:
                api_response = api_instance.get_namespaced_custom_object(group,
                                                                         version,
                                                                         namespace,
                                                                         plural,
                                                                         body['metadata']['name'])
            
                if 'status' in api_response and 'conditions' in api_response['status'] and api_response['status']['conditions']:
                    print([condition for condition in api_response['status']['conditions']])
                    if any(condition['type'] == 'Ready' and condition['status'] == 'True' for condition in api_response['status']['conditions']):
                        print('Inferenceservice successfully deployed')
                        break
                        
                cnt += 1
                time.sleep(sleep_interval)
                
            except:
                print('Inferenceservice failed deploying')
                assert False
                
        time.sleep(sleep_interval * 2)
        
        expected_result = {'predictions': [{'scores': [0.999114931, 9.20989623e-05, 0.000136786606, 0.000337258185, 0.000300533167, 1.84814126e-05], 'prediction': 0, 'key': '   1'}]}

        url = 'http://flower-sample.dgolubov.svc.cluster.local/v1/models/flower-sample:predict'
        input_content = read_content_from_url('https://gitlab.cern.ch/ai-ml/examples/-/raw/master/serving/single-model/input.json')
        r = requests.post(url, data=input_content)
        
        if r.json() != expected_result:
            assert False
        
        try:
            api_instance.delete_namespaced_custom_object(group,
                                                         version,
                                                         namespace,
                                                         plural,
                                                         body['metadata']['name'])
        except:
            print('Inferenceservice not deleted')
            assert False
            
        print('Inferenceservice deleted')
        assert True
        
    @pytest.mark.trainingoperators
    @pytest.mark.parametrize("url", training_operator_urls)
    def test_training_operator(self, url):
        content_yaml = read_content_from_url(url)
        body = yaml.load(content_yaml, yaml.FullLoader)
        
        api_instance = client.CustomObjectsApi()
        plural = body['kind'].lower() + 's'
        group = 'kubeflow.org'
        version = 'v1'
        namespace = 'dgolubov'
        
        try:
            api_response = api_instance.create_namespaced_custom_object(group, 
                                                                    version, 
                                                                    namespace, 
                                                                    plural,
                                                                    body)
        except Exception as e:
            print(body['kind'], 'not created')
            assert False
            
        print(body['kind'], 'created')
        
        cnt = 0
        
        while cnt < sleep_multplier:
            try:
                api_response = api_instance.get_namespaced_custom_object(group,
                                                                         version,
                                                                         namespace,
                                                                         plural,
                                                                         body['metadata']['name'])
            
                if 'status' in api_response and 'conditions' in api_response['status'] and api_response['status']['conditions']:
                    print([condition for condition in api_response['status']['conditions']])
                    if any(condition['type'] == 'Succeeded' and condition['status'] == 'True' for condition in api_response['status']['conditions']):
                        print(body['kind'], 'succeeded')
                        break
                        
                cnt += 1
                time.sleep(sleep_interval)
                
            except:
                print(body['kind'], 'failed')
                assert False
        
        print(cnt)
        if cnt >= sleep_multplier:
            print(body['kind'], 'did not succeed in', (sleep_interval * sleep_multplier), 'seconds')
            assert False
        
        try:
            api_instance.delete_namespaced_custom_object(group,
                                                         version,
                                                         namespace,
                                                         plural,
                                                         body['metadata']['name'])
        except:
            print(body['kind'], 'not deleted')
            assert False

        print(body['kind'], 'deleted')
        assert True
